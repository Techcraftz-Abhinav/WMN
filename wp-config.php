<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wmn');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8$W$oV/- {v|ABTq=>o)G4&:t8~]oJgW0}il1,4q:i%C8+b$v Ai{-S1$j t>>4s');
define('SECURE_AUTH_KEY',  'BiG$Y@(jZ$uTg@I-r`)KHmN}ywzX(?AV6Qv0M%<26P4Cbg~1q8hDo*P70<jT/o@D');
define('LOGGED_IN_KEY',    '(`&}n%iTy^>H[WhO1m8VpRhj7Chwb*Hx+(0ujC+=WKR2J2C*Gl&,-Z?5M8);E_nJ');
define('NONCE_KEY',        '`U.sUfy1YUL%`2KX2cQVeavr<:,rNVc.Nn!ce:;=t} Ce;;&{rYbU8#-n?0](9mP');
define('AUTH_SALT',        '8CP.pCP [:IB:[k`3#tZT2aR.u(FmZTRN)<x%KGN#2sFP=g9xT$t(D:mq6ePE5RN');
define('SECURE_AUTH_SALT', 'b-QRx(l[.!: CK .0K@1AvXO(*s|5ca|*L0$uUE|mXqe5VA?d8FTsKNN;lUIII{O');
define('LOGGED_IN_SALT',   '[)O0)z@GocOCm(nn^Fpy1+wWkOjJ{k,qEAhJ#(;u a^[X4H`KH_<I~#Fa l&Vm_S');
define('NONCE_SALT',       'PZRol?VMt{R>zwUApD]PChfO#w]e=OvIZ<l{~-28M9# ENO+s{21Ta1-ioch+ cz');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
