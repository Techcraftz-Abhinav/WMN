<?php get_header(); ?>

<style>
.featured_section {
	background:url('<?php echo get_stylesheet_directory_uri(); ?>/assets/images/home-feature.png') no-repeat;
	background-position: top center;
    background-size: cover;
}
</style>

<!------------ Feautred SECTION IS HERE -------------->

<section class="featured_section">
	<div class="container">
		<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon_cols">
			<div class="inner_content">
                <h1>women supporting women</h1>   
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit eu vulpate.</p>
			</div>
		</div>
		</div>
	</div>
</section>

<!---------- 3 icon Section --------------->
<section class="icon_section">
	<div class="container">
		<div class="row">
		<div class="icon_grid">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 icon_cols">
				<div class="icon_parent">
					<div class="icon_img">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon-1.png">
					</div>
					<h4><strong>wmn legal</strong></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 icon_cols">
				<div class="icon_parent">
					<div class="icon_img">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon-1.png">
					</div>
					<h4><strong>wmn FINANCE</strong></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 icon_cols">
				<div class="icon_parent">
					<div class="icon_img">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon-1.png">
					</div>
				<h4><strong>wmn DOCTOR</strong></h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</div>
			</div>
			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 btn_cols">
			<div class="icon_btn">
				<a href="javascript:void(0);">Get Matched Now</a>
			</div>
			</div>
		</div>
		</div>
	</div>
</section>


<!---------- who cares Section --------------->
<section class="care_section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="left_img">
					<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/who-cares.png">
				</div>
				<div class="right_content">
					<h4><strong>women supporting women</strong></h4>
					<h2>WORK WITH SOMEONE WHO CARES</h2>
					<hr>
					<p>Consectetur adipiscing elit.</p> 
					<p>Nulla quam velit eu vulpate.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<!---------- why we exist Section --------------->
<section class="exist_section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="left_exist_content">
					<h4><strong>WHY WE EXIST</strong></h4>
					<h3>We curate highly recommended, experienced resources for professional women so they can find and connect with the right resources for their needs.</h3>
					<p>We curate highly recommended, experienced resources for professional women so they can find and connect with the right resources for their needs.</p>
					<div class="exist_btn">
						<a href="javascript:void(0);">About us</a>
					</div>
				</div>
				<div class="right_exist_img">
					<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/exist.png">	
				</div>
			</div>
		</div>
	</div>
</section>


<!---------- why we exist Section --------------->
<section class="your_ques_section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ques_title">
					<h2>Your questions answered</h2>
					<h3>advice from the experts</h3>
				</div>
				<ul class="grid_parent">
					<li class="post_grid">
					<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ques-1.png">	
					<div class="grid_innercontent">
					<h4>TIPS AND TRICKS</h4>
					<h3>The Key to Finding Your Best Legal Ipsum</h3>
					</div>
					</li>
					
				</ul>
				<div class="exist_btn">
					<a href="javascript:void(0);">Read more from the blog</a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>