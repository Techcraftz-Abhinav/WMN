<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--link rel="profile" href="http://gmpg.org/xfn/11"-->
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header>
	<div class="header_top_part">
		<div class="container">
			<div class="row">				    
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="menu_inner">
						<div class="logo">
							<?php if(ot_get_option("logo")) { ?>
								<a href="<?php bloginfo('url'); ?>">
									<img class="img-responsive" src="<?php echo ot_get_option("logo"); ?>" alt="">
								</a>
							<?php } ?>
						</div>
						<nav class="main-menu">
						<?php 	$menu_args = array(
								'theme_location'	=> 'main-menu',
								'menu_class'=> 'slimmenu content mCustomScrollbar'
								);
								wp_nav_menu( $menu_args); 
							?>
						</nav>
					</div>	
				</div>
			</div>
		</div>
	</div>
</header>