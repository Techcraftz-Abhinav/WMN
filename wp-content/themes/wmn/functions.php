<?php 

function enqueue_jquery() {
   wp_enqueue_script('jquery');
}

/* add_action('wp_enqueue_scripts', 'enqueue_jquery'); */
// including various CSS and JS files
function my_theme_enqueue_styles() {
    // wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
  
    wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri() . '/assets/css/bootstrap.min.css');
    wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/assets/css/style.css');
	wp_enqueue_style( 'silm-menu', get_stylesheet_directory_uri() . '/assets/css/slimmenu.min.css');
	
	wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() . '/assets/js/bootstrap.min.js','','',true);
	wp_enqueue_script( 'slimmenu', get_stylesheet_directory_uri() . '/assets/js/jquery.slimmenu.min.js','','',true);
	 wp_enqueue_script( 'custom', get_stylesheet_directory_uri() . '/assets/js/custom.js','','',true);
	 
}

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles'); 

 // function to register the menu names 
add_action( 'init', 'register_my_menus' );
function register_my_menus() {
  register_nav_menus(
    array(
			'main-menu' => __( 'Main Menu'),
			'footer-menu' => __( 'Footer Menu'),
		)
	);
} 